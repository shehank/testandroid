package com.shehancom.nestedscrollview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;

public class MainActivity2 extends AppCompatActivity {
    RecyclerView rvNumbers;
    NestedScrollView nestedscroll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String [] numbers = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};

//        rvNumbers = (RecyclerView) findViewById(R.id.rv_numbers);
//        nestedscroll =  findViewById(R.id.nestedscroll);
        AppBarLayout my_appbar_container =  findViewById(R.id.my_appbar_container);
//        nestedscroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                my_appbar_container.setTranslationY(-scrollY);
//            }
//        });
//        rvNumbers.setLayoutManager(new LinearLayoutManager(this));
//        rvNumbers.setAdapter(new RecyclerViewAdapter(numbers));
    }
    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
        String listData[];

        public RecyclerViewAdapter(String data[]){
            this.listData = data;
        }

        @Override
        public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_item, parent, false);
            RecyclerViewAdapter.ViewHolder vh = new RecyclerViewAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
            holder.tvNumber.setText(listData[position]);
        }

        @Override
        public int getItemCount() {
            return listData.length;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvNumber;
            public ViewHolder(View v) {
                super(v);
                tvNumber = (TextView) v.findViewById(R.id.contact_name);
            }
        }
    }
}