package com.shehancom.nestedscrollview.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.shehancom.nestedscrollview.MyRecyclerViewAdapter;
import com.shehancom.nestedscrollview.R;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = new ViewModelProvider(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView recyclerView= (RecyclerView) root.findViewById(R.id.recycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ArrayList<String> strings = new ArrayList<>();
        strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga");
        strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga");
        strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga"); strings.add("shehan");
        strings.add("karunarathna");
        strings.add("hasanga");
        MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(getActivity(),strings);
        recyclerView.setAdapter(adapter);
//        pageViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return root;
    }
}